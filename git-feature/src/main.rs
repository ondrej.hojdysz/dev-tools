use clap::{Args, Parser, Subcommand};
use conflib::ConfLoader;
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};

fn main() {
    let args = CmdArgs::parse();
    let config: Config = ConfLoader::new(
        PathBuf::from("git-feature.toml"),
        "dev-tools".to_string(),
        true,
    )
    .load_config()
    .unwrap();

    match args.command {
        Commands::Create(args) => {
            create_feature(Context::from((config, args)));
        }
        Commands::Erase(args) => {
            erase_feature(Context::from((config, args)));
        }
        Commands::Merge(args) => {
            merge_feature(Context::from((config, args)));
        }
        Commands::Finish(args) => {
            finish_feature(Context::from((config, args)));
        }
    }
}

struct Context<T> {
    feature: String,
    args: T,
}

#[derive(Deserialize, Serialize)]
struct Config {
    prefix: Option<String>,
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct CmdArgs {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    #[command(arg_required_else_help = true)]
    Create(CreateArgs),
    Erase(EraseArgs),
    Merge(MergeArgs),
    Finish(FinishArgs),
}

#[derive(Args, Debug)]
struct CreateArgs {
    feature: String,
    source: Option<String>,
}

impl From<(Config, CreateArgs)> for Context<CreateArgs> {
    fn from(value: (Config, CreateArgs)) -> Self {
        let (config, args) = value;
        let branchname = if config
            .prefix
            .as_ref()
            .is_some_and(|prefix| !prefix.is_empty())
        {
            format!("{}{}", config.prefix.as_ref().unwrap(), args.feature)
        } else {
            args.feature.to_string()
        };
        Self {
            feature: branchname,
            args,
        }
    }
}

#[derive(Args, Debug)]
struct EraseArgs {
    feature: Option<String>,
    #[arg(long, short, default_value = "origin")]
    remote: String,
}

impl From<(Config, EraseArgs)> for Context<EraseArgs> {
    fn from(value: (Config, EraseArgs)) -> Self {
        let (config, args) = value;
        if let Some(feature) = args.feature.clone() {
            let branchname = if config
                .prefix
                .as_ref()
                .is_some_and(|prefix| !prefix.is_empty())
            {
                format!("{}{}", config.prefix.as_ref().unwrap(), feature)
            } else {
                feature
            };
            return Self {
                feature: branchname,
                args,
            };
        }
        Self {
            feature: branch_fuzzy_search(),
            args,
        }
    }
}

#[derive(Args, Debug)]
struct FinishArgs {
    target: Option<String>,
    #[arg(long, short, default_value = "origin")]
    remote: String,
}

impl From<(Config, FinishArgs)> for Context<FinishArgs> {
    fn from(value: (Config, FinishArgs)) -> Self {
        let (_config, args) = value;
        let branchname = git_wrapper::get_current_branch().unwrap();

        Self {
            feature: branchname,
            args,
        }
    }
}

#[derive(Args, Debug)]
struct MergeArgs {
    feature: Option<String>,
}

impl From<(Config, MergeArgs)> for Context<MergeArgs> {
    fn from(value: (Config, MergeArgs)) -> Self {
        let (config, args) = value;
        if let Some(feature) = args.feature.clone() {
            let branchname = if config
                .prefix
                .as_ref()
                .is_some_and(|prefix| !prefix.is_empty())
            {
                format!("{}{}", config.prefix.as_ref().unwrap(), feature)
            } else {
                feature
            };
            return Self {
                feature: branchname,
                args,
            };
        }
        Self {
            feature: branch_fuzzy_search(),
            args,
        }
    }
}

fn create_feature(context: Context<CreateArgs>) {
    println!("Creating feature: {}", context.feature);
    git_wrapper::fetch(&["--all"]).unwrap();
    let source = if let Some(source) = context.args.source {
        source
    } else {
        branch_fuzzy_search()
    };
    let remotes = git_wrapper::get_remotes().unwrap();

    let mut starts_with_remote = false;
    for remote in remotes {
        if source.starts_with(&remote) {
            starts_with_remote = true;
            break;
        }
    }

    if starts_with_remote {
        git_wrapper::switch(&["-c", &context.feature, &source]).unwrap();
    } else {
        git_wrapper::switch(&[&source]).unwrap();
        if let Err(e) = git_wrapper::pull(&[]) {
            eprintln!("Pulling source branch failed: {e}.\nUsing local copy.");
        }
        git_wrapper::switch(&["-c", &context.feature]).unwrap();
    }

    if Path::new(".gitmodules").exists() {
        println!("Updatins submodules");
        git_wrapper::init_submodules().unwrap();
    }
}

fn erase_feature(context: Context<EraseArgs>) {
    erase(&context.feature, &context.args.remote);
}

fn merge_feature(context: Context<MergeArgs>) {
    merge(&context.feature);
}

fn finish_feature(context: Context<FinishArgs>) {
    println!("Finishing feature: {}", context.feature);
    let target = if let Some(target) = context.args.target {
        target
    } else {
        branch_fuzzy_search()
    };
    git_wrapper::switch(&[&target]).unwrap();
    git_wrapper::pull(&[]).unwrap();
    merge(&context.feature);
    erase(&context.feature, &context.args.remote);
}

fn merge(name: &str) {
    git_wrapper::merge(&[name]).unwrap();
}

fn erase(branch: &str, remote: &str) {
    println!("Deleting local branch: {}", branch);
    if let Err(error) = git_wrapper::branch(&["-D", branch]) {
        println!("Deleting local branch failed: {:?}", error);
    }

    println!("Deleting remote branch: {}", &branch);
    if let Err(error) = git_wrapper::push(&[remote, "-d", branch]) {
        println!("Deleting remote branch failed: {:?}", error);
    }
}

fn branch_fuzzy_search() -> String {
    let branches = git_wrapper::get_branches().unwrap();
    if let Some(value) = tui_libs::widgets::fuzzy_select_dialog::run_fuzzy_dialog(branches).unwrap()
    {
        value
    } else {
        panic!("You must specify source reference!");
    }
}
