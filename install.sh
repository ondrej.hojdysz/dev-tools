#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cargo test --release

for executable in $(find . -name main.rs | sed -re "s/^\.\/(.*)/\1/" -e "s_/.*__")
do
  echo installing ${executable}
  cargo install --path $executable
done
