use clap::Parser;

#[derive(Debug, Parser)]
struct Opts {
    #[arg(long, short, default_value = "origin")]
    remote: String,
}
fn main() {
    let opts = Opts::parse();
    if let Err(error) = git_wrapper::push(&["-u", &opts.remote]) {
        println!("Publishing failed: {:?}", error);
    }
}
