use std::{
    path::Path,
    process::{exit, Command},
    sync::mpsc::Receiver,
    thread::sleep,
    time::Duration,
};

use thiserror::Error;

use ignore::Walk;
use notify::{EventKind, RecursiveMode, Watcher};

use clap::Parser;

type Result<T> = std::result::Result<T, Errors>;

#[derive(Debug, Error)]
enum Errors {
    #[error(transparent)]
    Notify(#[from] notify::Error),
    #[error(transparent)]
    IO(#[from] std::io::Error),
    #[error("{msg}")]
    General { msg: String },
}

#[derive(Parser, Debug)]
struct Args {
    #[clap(
        long,
        short,
        help = "List of files or directories to watch",
        value_delimiter = ','
    )]
    watchlist: Vec<std::path::PathBuf>,
    #[clap(trailing_var_arg = true, allow_hyphen_values = true)]
    command: Vec<String>,

    #[clap(
        short,
        long,
        help = "Timeout in seconds before command is run",
        default_value = "1"
    )]
    timeout: u64,

    #[clap(
        long,
        short,
        help = "Shell that runs the commands after file change, if not set SHELL variable is used and arg '-c' is added"
    )]
    shell: Option<String>,

    #[clap(
        long,
        short = 'a',
        help = "Arguments for the shell, ignored if shell is not set"
    )]
    shellargs: Option<Vec<String>>,
}
fn main() {
    let args = Args::parse();
    if args.command.is_empty() {
        eprintln!("Command cannot be empty?");
        exit(1);
    }

    for path in &args.watchlist {
        if !path.exists() {
            eprintln!("Path does not exists: {path:#?}!");
            exit(1);
        }
    }

    let shell = if args.shell.is_none() {
        std::env::var("SHELL").expect("Unable to set shell")
    } else {
        args.shell.as_ref().unwrap().clone()
    };

    let mut command = std::process::Command::new(shell);
    if let Some(args) = &args.shellargs {
        for arg in args {
            command.arg(arg);
        }
    } else {
        command.arg("-c");
    }
    command.arg(if args.command.len() > 1 {
        let mut command = String::new();
        for arg in &args.command {
            command += arg;
            command += " "
        }

        command
    } else {
        args.command[0].clone()
    });

    loop {
        match run_watcher(&mut command, &args) {
            Ok(_) => {
                break;
            }
            Err(e) => {
                eprintln!(
                    "Watcher encountered error: {e}, restarting in {} seconds...",
                    args.timeout
                );
                sleep(Duration::from_secs(args.timeout));
            }
        }
    }
}

fn run_watcher(command: &mut Command, args: &Args) -> Result<()> {
    println!("Running command: {:#?}", command);
    let notify = || {
        println!("\n----------\nWaiting for changes...\n\n");
    };

    let (tx, rx) = std::sync::mpsc::channel();
    let mut watcher = notify::recommended_watcher(tx)?;

    notify();
    create_watches(&mut watcher, args)?;
    loop {
        let mut runcommand = false;
        while let Ok(event) = rx.recv_timeout(Duration::from_secs(args.timeout)) {
            let event = event?;
            match event.kind {
                EventKind::Modify(_) => {
                    runcommand = true;
                }
                EventKind::Create(_) => {
                    println!("Adding new entries");
                    for path in &event.paths {
                        add_watches(&mut watcher, path)?;
                    }
                    runcommand = true;
                }
                EventKind::Remove(event) => {
                    println!("Some watched entries were removed: {:?}", event);
                    runcommand = true;
                }
                _ => {}
            }
        }

        if runcommand {
            command.spawn()?.wait()?;
            notify();
        }
    }
}

fn create_watches<W: notify::Watcher>(watcher: &mut W, args: &Args) -> Result<()> {
    let mut atleast1 = false;
    for watch in &args.watchlist {
        if watch.exists() {
            add_watches(watcher, watch)?;
            atleast1 = true;
        }
    }

    if !atleast1 {
        eprintln!("All watched files/directorise removed, quiting!");
        exit(1);
    }

    Ok(())
}

fn add_watches<W: notify::Watcher, P: AsRef<Path> + std::fmt::Debug>(
    watcher: &mut W,
    path: P,
) -> Result<()> {
    println!("Adding watch for: {:?}", path);
    for entry in Walk::new(path).flatten() {
        if entry.path().is_symlink() {
            if let Ok(linkpath) = entry.path().read_link() {
                if !linkpath.exists() {
                    continue;
                }
            } else {
                continue;
            }
        }
        watcher.watch(entry.path(), RecursiveMode::NonRecursive)?;
    }

    Ok(())
}
