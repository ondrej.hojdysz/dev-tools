use std::boxed::Box;
use thiserror::Error;

use git2::Repository;

pub type Git2Result = std::result::Result<(), Errors>;

// pub mod fetch;
// pub mod merge;
// pub mod switch;

#[derive(Error, Debug)]
pub enum Errors {
    #[error(transparent)]
    Git2(#[from] git2::Error),
    #[error(transparent)]
    IO(#[from] std::io::Error),
    #[error("Unable to fast forward branch: {branch}")]
    FastForwardFailed { branch: String },
    #[error("Branch {branch} does not exists")]
    BranchDoesNotExists { branch: String },
    #[error("Branch {branch} already exists")]
    BranchAlreadyExists { branch: String },
    #[error("Currently there is no branch checkouted")]
    NotOnBranch,
    #[error(transparent)]
    FromUtf(#[from] std::string::FromUtf8Error),
    #[error("Detatched head")]
    DetatchedHead,
    #[error("Git command failed!")]
    CommandFailed,
}

fn spawn_process(command: &str, args: &[&str]) -> Git2Result {
    let mut child = std::process::Command::new("git")
        .arg(command)
        .args(args)
        .spawn()?;

    let output = child.wait()?;
    if !output.success() {
        return Err(Errors::CommandFailed);
    }

    Ok(())
}

pub fn fetch(args: &[&str]) -> Git2Result {
    spawn_process("fetch", args)
}

pub fn push(args: &[&str]) -> Git2Result {
    spawn_process("push", args)
}

pub fn pull(args: &[&str]) -> Git2Result {
    spawn_process("pull", args)
}

pub fn clone(args: &[&str]) -> Git2Result {
    spawn_process("clone", args)
}

pub fn init(args: &[&str]) -> Git2Result {
    spawn_process("init", args)
}

pub fn commit(args: &[&str]) -> Git2Result {
    spawn_process("commit", args)
}

pub fn switch(args: &[&str]) -> Git2Result {
    spawn_process("switch", args)
}

pub fn add(args: &[&str]) -> Git2Result {
    spawn_process("add", args)
}

pub fn branch(args: &[&str]) -> Git2Result {
    spawn_process("branch", args)
}

pub fn merge(args: &[&str]) -> Git2Result {
    spawn_process("merge", args)
}

pub fn rev_parse(args: &[&str]) -> Git2Result {
    spawn_process("rev-parse", args)
}

pub fn submodule(args: &[&str]) -> Git2Result {
    spawn_process("submodule", args)
}

pub fn init_submodules() -> Git2Result {
    submodule(&["update", "--init"])
}

// pub fn reference_exists(gref: &str) -> std::result::Result<bool, Errors> {
//     let repo = Repository::open(".")?;
//     let output = repo.resolve_reference_from_short_name(gref).is_ok();

//     Ok(output)
// }

// pub fn branch_exists(branch: &str) -> std::result::Result<bool, Errors> {
//     let repo = Repository::open(".")?;
//     let output = repo.find_branch(branch, git2::BranchType::Local).is_ok()
//         || repo
//             .find_branch(&format!("origin/{branch}"), git2::BranchType::Remote)
//             .is_ok();

//     print_output(crate::branch(&[&"-a"]).unwrap()).unwrap();
//     println!("Search for {branch} result: {output}");
//     Ok(output)
// }

pub fn get_current_branch() -> std::result::Result<String, Errors> {
    let repo = Repository::open(".")?;
    let head = repo.head()?;
    if !head.is_branch() {
        return Err(Errors::NotOnBranch);
    }

    Ok(bytes_to_string(head.name_bytes())?
        .strip_prefix("refs/heads/")
        .unwrap()
        .to_string())
}

fn bytes_to_string(bytes: &[u8]) -> std::result::Result<String, Errors> {
    Ok(String::from_utf8(bytes.to_vec())?)
}

pub fn get_branches() -> std::result::Result<Vec<String>, Errors> {
    let repo = Repository::open(".")?;
    let branches = repo
        .branches(None)?
        .map(|branchres| {
            let branch = branchres?;
            let branchname = bytes_to_string(branch.0.name_bytes()?)?;
            Ok(branchname)
        })
        .map(|branch: std::result::Result<String, Box<dyn std::error::Error>>| branch.unwrap())
        .collect();
    Ok(branches)
}

pub fn get_remotes() -> std::result::Result<Vec<String>, Errors> {
    let repo = Repository::open(".")?;
    let remotes = repo
        .remotes()?
        .iter()
        .map(|remote| remote.unwrap().to_string())
        .collect();
    Ok(remotes)
}
#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::{read_to_string, File};
    use std::io::Write;
    use std::path::PathBuf;
    use tempdir::TempDir;

    const REMOTE: &str = "repo";

    struct RepoConfig {
        remote: PathBuf,
        tempdir: TempDir,
    }

    #[test]
    fn tests() {
        const CONTENT: &str = "Hello world";
        const TESTFILE: &str = "hello.txt";
        let remote = set_up_remote("test");
        let pushrepo = set_up_repo("push", &remote);
        let pullrepo = set_up_repo("pull", &remote);

        assert!(remote.remote.exists());
        assert!(pushrepo.exists());
        assert!(pullrepo.exists());

        std::env::set_current_dir(pushrepo.clone()).unwrap();
        println!("{:?}", switch(&["-c", "master"]).unwrap());
        let mut f = File::create(pushrepo.join(TESTFILE)).unwrap();
        f.sync_all().unwrap();
        println!("{:?}", add(&[TESTFILE]).unwrap());
        println!("{:?}", commit(&["-am", "initial"]).unwrap());
        println!("{:?}", switch(&["-c", "throwaway"]).unwrap());
        f.write_all(CONTENT.as_bytes()).unwrap();
        f.sync_all().unwrap();
        println!("{:?}", add(&[TESTFILE]).unwrap());
        println!("{:?}", commit(&["-am", "testing push"]).unwrap());
        println!("{:?}", switch(&["master"]).unwrap());
        println!("{:?}", merge(&["throwaway"]).unwrap());
        println!("{:?}", branch(&["-D", "throwaway"]).unwrap());
        println!("{:?}", push(&[]).unwrap());

        std::env::set_current_dir(pullrepo.clone()).unwrap();
        println!("{:?}", switch(&["-c", "master"]).unwrap());
        println!("{:?}", pull(&[]));
        assert_eq!("master", get_current_branch().unwrap());

        let content = read_to_string(pullrepo.join(TESTFILE)).unwrap();
        assert_eq!(content, CONTENT);
        println!("TmpDir path: {:?}", remote.tempdir);
    }

    fn set_up_remote(prefix: &str) -> RepoConfig {
        let dir = TempDir::new(prefix).unwrap();

        let repo = dir.path().join(REMOTE);
        println!("{:?}", init(&["--bare", repo.to_str().unwrap()]).unwrap());

        RepoConfig {
            remote: repo,
            tempdir: dir,
        }
    }

    fn set_up_repo(dirname: &str, remote: &RepoConfig) -> PathBuf {
        let path = remote.tempdir.path().join(dirname);
        println!(
            "{:?}",
            clone(&[remote.remote.to_str().unwrap(), path.to_str().unwrap()]).unwrap()
        );

        path
    }
}
