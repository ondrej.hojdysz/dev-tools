use thiserror::Error;

pub type ExecResult<T> = std::result::Result<T, Errors>;

#[derive(Error, Debug)]
pub enum Errors {
    #[error(transparent)]
    ConfError(#[from] conflib::ConfigError),
    #[error(transparent)]
    IOError(#[from] std::io::Error),
    #[error(transparent)]
    RegexError(#[from] regex::Error),
    #[error(transparent)]
    StorageError(#[from] storage::Error),
    #[error("Buildfile or dependson must be set!")]
    BuildfileOrDependsonNotSet,
    #[error("Buildfile does not exists: {file}!")]
    BuildfileDoesNotExists { file: std::path::PathBuf },
    #[error("Name not set!")]
    NameNotSet,
    #[error("Backend not set!")]
    BackendNotSet,
    #[error("{name} command is not set!")]
    CommandEmpty { name: String },
    #[error("Command: {command}, exited with code: {code:?}")]
    CommandRunFailed { code: Option<i32>, command: String },
    #[error("Workdir must be set if buildfile is not absolute path.")]
    WorkdirNotSet,
    #[error("Workdir must be absolute path")]
    WorkdirMustBeAbsolute,
    #[error("Unexpected list images output: {output}")]
    UnexpectedDockerOutput { output: String },
    #[error("Dependency not loaded")]
    DependencyNotLoaded,
}
