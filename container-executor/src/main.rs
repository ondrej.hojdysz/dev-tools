use clap::{Args, Parser, Subcommand};
use container_executor_lib::container::Container;

fn main() {
    let args = CmdArgs::parse();
    if let Commands::CreateDefaultBackends = args.command.clone() {
        save_default_backends();
        return;
    }

    if args.name.is_none() {
        eprintln!("Name must be set for all commands except for create-default-args");
        std::process::exit(42);
    }

    let container = Container::new(args.name.as_ref().unwrap()).unwrap();

    match args.command.clone() {
        Commands::Build(buildargs) => {
            build(&container, &Context::from((args, buildargs)));
        }
        Commands::Start(runargs) => {
            start(&container, &Context::from((args, runargs)));
        }
        Commands::Stop(runargs) => {
            stop(&container, &Context::from((args, runargs)));
        }
        Commands::Exec(runargs) => {
            exec(&container, &Context::from((args, runargs)));
        }
        Commands::Clean(cleanargs) => {
            clean(&container, &Context::from((args, cleanargs)));
        }
        Commands::CreateDefaultBackends => {}
    }
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct CmdArgs {
    #[command(subcommand)]
    command: Commands,
    #[arg(short, long)]
    name: Option<String>,
}

#[derive(Clone, Debug, Subcommand)]
enum Commands {
    Build(BuildArgs),
    Start(StartArgs),
    Stop(StopArgs),
    Exec(ExecArgs),
    Clean(CleanArgs),
    CreateDefaultBackends,
}

#[derive(Args, Debug, Clone)]
struct BuildArgs {
    #[clap(long, short)]
    force: bool,
}

#[derive(Args, Debug, Clone)]
struct StartArgs {
    #[clap(long, short)]
    rebuildimage: bool,
}

#[derive(Args, Debug, Clone)]
struct StopArgs {}

#[derive(Args, Debug, Clone)]
struct ExecArgs {
    execargs: Vec<String>,
    #[clap(long, short)]
    rebuildimage: bool,
}

#[derive(Args, Debug, Clone)]
struct CleanArgs {
    #[clap(long, short)]
    recursive: bool,
}

struct Context<T> {
    args: T,
}

impl From<(CmdArgs, BuildArgs)> for Context<BuildArgs> {
    fn from(value: (CmdArgs, BuildArgs)) -> Self {
        Self { args: value.1 }
    }
}

impl From<(CmdArgs, StartArgs)> for Context<StartArgs> {
    fn from(value: (CmdArgs, StartArgs)) -> Self {
        Self { args: value.1 }
    }
}

impl From<(CmdArgs, StopArgs)> for Context<StopArgs> {
    fn from(value: (CmdArgs, StopArgs)) -> Self {
        Self { args: value.1 }
    }
}

impl From<(CmdArgs, ExecArgs)> for Context<ExecArgs> {
    fn from(value: (CmdArgs, ExecArgs)) -> Self {
        Self { args: value.1 }
    }
}

impl From<(CmdArgs, CleanArgs)> for Context<CleanArgs> {
    fn from(value: (CmdArgs, CleanArgs)) -> Self {
        Self { args: value.1 }
    }
}

fn build(container: &Container, context: &Context<BuildArgs>) {
    container_executor_lib::build(container, context.args.force).unwrap();
}

fn start(container: &Container, context: &Context<StartArgs>) {
    container_executor_lib::start(container, context.args.rebuildimage).unwrap();
}

fn stop(container: &Container, _context: &Context<StopArgs>) {
    container_executor_lib::stop(container).unwrap();
}

fn exec(container: &Container, context: &Context<ExecArgs>) {
    container_executor_lib::exec(container, context.args.rebuildimage, &context.args.execargs)
        .unwrap();
}

fn clean(container: &Container, context: &Context<CleanArgs>) {
    container_executor_lib::clean(container, context.args.recursive).unwrap()
}

fn save_default_backends() {
    let store = storage::StorageBuilder::new("dev-tools").build().unwrap();
    container_executor_lib::save_default_backends(&store).unwrap();
}
