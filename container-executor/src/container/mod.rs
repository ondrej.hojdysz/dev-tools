pub mod backend;
pub mod container_configuration;

use std::boxed::Box;
use std::rc::Rc;

use crate::errors::{Errors, ExecResult};
use backend::Backend;

type ReplaceMap = std::collections::HashMap<String, String>;

pub fn save_default_backends(store: &storage::Storage) -> ExecResult<()> {
    backend::save_default_backends(store)
}

pub struct Container {
    config: container_configuration::ContainerConfiguration,
    backend: backend::Backend,
    storage: Rc<storage::Storage>,
    dependency: Option<Box<Container>>,
}

impl Container {
    pub fn new(name: &str) -> ExecResult<Self> {
        let storage = storage::StorageBuilder::new("dev-tools").build()?;
        Self::new_with_storage(name, Rc::new(storage))
    }

    pub fn new_with_storage(name: &str, store: Rc<storage::Storage>) -> ExecResult<Self> {
        let config = container_configuration::ContainerConfiguration::new(name, &store)?;
        Ok(Self {
            backend: Backend::new(&config.backend, &store)?,
            storage: store.clone(),
            dependency: if let Some(dep) = config.dependson.as_ref() {
                Some(Box::new(Self::new_with_storage(dep, store)?))
            } else {
                None
            },
            config,
        })
    }

    pub fn image_exists(&self) -> ExecResult<bool> {
        self.backend.image_exists(self)
    }

    pub fn image_build(&self) -> ExecResult<()> {
        if let Some(dep) = self.dependency.as_ref() {
            dep.image_build()?;
        }

        if let Some(workdir) = self.config.workdir.as_ref() {
            std::env::set_current_dir(workdir)?
        }

        if self.config.buildfile.is_some() {
            self.backend.image_build(self)
        } else {
            Ok(())
        }
    }

    pub fn start(&self) -> ExecResult<()> {
        if let Some(workdir) = self.config.workdir.as_ref() {
            std::env::set_current_dir(workdir)?
        }

        self.backend.start(self)
    }

    pub fn is_running(&self) -> ExecResult<bool> {
        self.backend.container_running(self)
    }

    pub fn stop(&self) -> ExecResult<()> {
        self.backend.stop(self)
    }

    pub fn exec<T: AsRef<str>>(&self, args: &[T]) -> ExecResult<()> {
        if let Some(workdir) = self.config.workdir.as_ref() {
            std::env::set_current_dir(workdir)?
        }

        self.backend.exec(self, args)
    }

    pub fn image_clean(&self, recursive: bool) -> ExecResult<()> {
        if self.config.buildfile.is_some() {
            self.backend.image_clean(self)?;
        }
        if recursive {
            if let Some(dep) = self.dependency.as_ref() {
                dep.image_clean(recursive)?;
            }
        }
        Ok(())
    }

    pub fn get_storage(&self) -> Rc<storage::Storage> {
        self.storage.clone()
    }

    pub fn get_container_name(&self) -> &str {
        &self.config.name
    }

    pub fn get_replace_map(&self) -> ExecResult<std::collections::HashMap<String, String>> {
        let mut map = std::collections::HashMap::new();
        map.insert("%name%".to_string(), self.config.name.clone());
        map.insert(
            "%timestamp%".to_string(),
            chrono::Local::now().format("%y%m%d%H%M%S").to_string(),
        );
        map.insert(
            "%image_name%".to_string(),
            self.get_image_name()?.to_string(),
        );
        if let Some(buildfile) = &self.config.buildfile {
            if buildfile.is_absolute() {
                map.insert(
                    "%buildfile%".to_string(),
                    buildfile.to_string_lossy().to_string(),
                );

                if self.config.workdir.is_none() {
                    map.insert(
                        "%workdir%".to_string(),
                        buildfile.parent().unwrap().to_string_lossy().to_string(),
                    );
                }
            } else if let Some(workdir) = &self.config.workdir {
                map.insert(
                    "%buildfile%".to_string(),
                    workdir.join(buildfile).to_string_lossy().to_string(),
                );

                map.insert(
                    "%workdir%".to_string(),
                    workdir.to_string_lossy().to_string(),
                );
            }
        }

        Ok(map)
    }

    pub fn get_image_name(&self) -> ExecResult<&str> {
        if self.config.buildfile.is_some() {
            Ok(&self.config.name)
        } else if let Some(dep) = self.dependency.as_ref() {
            dep.get_image_name()
        } else {
            Err(Errors::DependencyNotLoaded)
        }
    }
}
