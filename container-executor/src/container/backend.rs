use crate::container::{Container, ReplaceMap};
use serde::{Deserialize, Serialize};
use std::process::{Command, Output};

use crate::errors::{Errors, ExecResult};

pub fn save_default_backends(store: &storage::Storage) -> ExecResult<()> {
    let docker = BackendConfig {
        build: CommandConfig {
            name: "docker".to_string(),
            args: [
                "build",
                "-t",
                "%image_name%:latest",
                "-t",
                "%image_name%:%timestamp%",
                "-f",
                "%buildfile%",
                "%workdir%",
            ]
            .into_iter()
            .map(String::from)
            .collect(),
        },
        clean: CommandConfig {
            name: "docker".to_string(),
            args: ["image", "rm", "--force"]
                .into_iter()
                .map(String::from)
                .collect(),
        },
        execute: CommandConfig {
            name: "docker".to_string(),
            args: ["exec", "-it", "%name%"]
                .into_iter()
                .map(String::from)
                .collect(),
        },
        start: CommandConfig {
            name: "docker".to_string(),
            args: ["run", "--rm", "-d", "--name", "%name%", "%image_name%"]
                .into_iter()
                .map(String::from)
                .collect(),
        },
        stop: CommandConfig {
            name: "docker".to_string(),
            args: ["stop", "%name%"].into_iter().map(String::from).collect(),
        },
        listimages: CommandConfig {
            name: "docker".to_string(),
            args: ["images", "%image_name%"]
                .into_iter()
                .map(String::from)
                .collect(),
        },
        listcontainers: CommandConfig {
            name: "docker".to_string(),
            args: ["ps"].into_iter().map(String::from).collect(),
        },
    };

    let output = store.get_config_dir().join("cont-exec").join("backends");

    if !output.exists() {
        std::fs::create_dir_all(output.clone())?;
    }

    storage::Storage::write_to_toml(output.join("docker.toml"), &docker)?;
    Ok(())
}

#[derive(Deserialize, Serialize)]
struct CommandConfig {
    name: String,
    #[serde(default = "default_args")]
    args: Vec<String>,
}

fn default_args() -> Vec<String> {
    Vec::new()
}

impl CommandConfig {
    pub fn get_runner(&self, replacemap: ReplaceMap) -> ExecResult<CommandRunner> {
        let mut runner = CommandRunner::new(self, replacemap);
        runner.add_arguments(&self.args)?;
        Ok(runner)
    }
}

struct CommandRunner {
    command: Command,
    replacemap: ReplaceMap,
}

impl CommandRunner {
    pub fn new(command: &CommandConfig, replacemap: ReplaceMap) -> Self {
        Self {
            command: Command::new(&command.name),
            replacemap,
        }
    }

    pub fn add_arguments<T: AsRef<str>>(&mut self, args: &[T]) -> ExecResult<&mut Self> {
        let re = regex::Regex::new(r"%[^%]+%")?;
        if !args.is_empty() {
            args.iter().for_each(|arg| {
                let mut finalarg = arg.as_ref().to_string();
                re.find_iter(arg.as_ref()).for_each(|item| {
                    let strmatch = item.as_str();
                    if let Some(value) = self.replacemap.get(strmatch) {
                        finalarg = finalarg.replace(strmatch, value);
                    }
                });

                self.command.arg(finalarg);
            });
        }

        Ok(self)
    }

    pub fn run(&mut self) -> ExecResult<()> {
        let status = self.command.status()?;

        if !status.success() {
            return Err(Errors::CommandRunFailed {
                code: status.code(),
                command: std::format!("{:?}", self.command),
            });
        }

        Ok(())
    }

    pub fn run_with_output(&mut self) -> ExecResult<Output> {
        let output = self.command.output()?;

        if !output.status.success() {
            return Err(Errors::CommandRunFailed {
                code: output.status.code(),
                command: std::format!("{:?}", self.command),
            });
        }

        Ok(output)
    }
}

#[derive(Deserialize, Serialize)]
pub struct BackendConfig {
    build: CommandConfig,
    start: CommandConfig,
    stop: CommandConfig,
    execute: CommandConfig,
    clean: CommandConfig,
    listimages: CommandConfig,
    listcontainers: CommandConfig,
}

pub struct Backend {
    config: BackendConfig,
}

macro_rules! validate_field {
    ($field:expr, $fieldname: tt) => {
        if $field.name.is_empty() {
            return Err(Errors::CommandEmpty {
                name: $fieldname.to_string(),
            });
        }
    };
}

impl Backend {
    pub fn new(name: &str, storage: &storage::Storage) -> ExecResult<Self> {
        Ok(Self {
            config: Self::load_config(name, storage)?,
        })
    }

    fn load_config(name: &str, storage: &storage::Storage) -> ExecResult<BackendConfig> {
        let path = std::path::PathBuf::new()
            .join("cont-exec")
            .join("backends")
            .join(name);
        let config = conflib::ConfLoader::new(path, "dev-tools".to_string(), true)
            .load_config_with_storage(storage)?;
        Self::validate_config(&config)?;
        Ok(config)
    }

    fn validate_config(config: &BackendConfig) -> ExecResult<()> {
        validate_field!(config.build, "Build");
        validate_field!(config.start, "Start");
        validate_field!(config.clean, "Clean");
        validate_field!(config.listimages, "Listimages");
        validate_field!(config.listcontainers, "Listcontainers");
        validate_field!(config.execute, "Execute");

        Ok(())
    }

    pub fn image_exists(&self, container: &Container) -> ExecResult<bool> {
        let output = self
            .config
            .listimages
            .get_runner(container.get_replace_map()?)?
            .run_with_output()?;

        Ok(String::from_utf8_lossy(&output.stdout).contains(&container.get_image_name()?))
    }

    pub fn image_build(&self, container: &Container) -> ExecResult<()> {
        self.config
            .build
            .get_runner(container.get_replace_map()?)?
            .run()
    }

    pub fn start(&self, container: &Container) -> ExecResult<()> {
        self.config
            .start
            .get_runner(container.get_replace_map()?)?
            .run()
    }

    pub fn stop(&self, container: &Container) -> ExecResult<()> {
        let output = self
            .config
            .listcontainers
            .get_runner(container.get_replace_map()?)?
            .run_with_output()?;

        let mut containers: Vec<String> = Vec::new();
        for cont in String::from_utf8_lossy(&output.stdout).lines() {
            if cont.contains(&container.get_container_name()) {
                let splitimage = cont
                    .split_whitespace()
                    .into_iter()
                    .filter(|split| !split.is_empty())
                    .collect::<Vec<&str>>();

                if splitimage.len() < 1 {
                    return Err(Errors::UnexpectedDockerOutput {
                        output: cont.to_string(),
                    });
                }

                containers.push(String::from(splitimage[0]));
            }
        }

        self.config
            .stop
            .get_runner(container.get_replace_map()?)?
            .add_arguments(&containers)?
            .run()
    }

    pub fn exec<T: AsRef<str>>(&self, container: &Container, args: &[T]) -> ExecResult<()> {
        self.config
            .execute
            .get_runner(container.get_replace_map()?)?
            .add_arguments(args)?
            .run()
    }

    pub fn image_clean(&self, container: &Container) -> ExecResult<()> {
        let output = self
            .config
            .listimages
            .get_runner(container.get_replace_map()?)?
            .run_with_output()?;

        let mut images = Vec::new();
        for image in String::from_utf8_lossy(&output.stdout).lines() {
            if image.contains(&container.get_image_name()?) {
                let splitimage = image
                    .split_whitespace()
                    .into_iter()
                    .filter(|split| !split.is_empty())
                    .collect::<Vec<&str>>();

                if splitimage.len() < 2 {
                    return Err(Errors::UnexpectedDockerOutput {
                        output: image.to_string(),
                    });
                }

                images.push(String::from(splitimage[0]) + ":" + splitimage[1]);
            }
        }

        if !images.is_empty() {
            self.config
                .clean
                .get_runner(container.get_replace_map()?)?
                .add_arguments(&images)?
                .run()
        } else {
            Ok(())
        }
    }

    pub fn container_running(&self, container: &Container) -> ExecResult<bool> {
        let output = self
            .config
            .listcontainers
            .get_runner(container.get_replace_map()?)?
            .run_with_output()?;

        for cont in String::from_utf8_lossy(&output.stdout).lines() {
            if cont.contains(&container.get_container_name()) {
                return Ok(true);
            }
        }
        Ok(false)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_config() -> BackendConfig {
        BackendConfig {
            build: CommandConfig {
                name: "docker".to_string(),
                args: Vec::new(),
            },
            start: CommandConfig {
                name: "docker".to_string(),
                args: Vec::new(),
            },
            stop: CommandConfig {
                name: "docker".to_string(),
                args: Vec::new(),
            },
            clean: CommandConfig {
                name: "docker".to_string(),
                args: Vec::new(),
            },
            listimages: CommandConfig {
                name: "docker".to_string(),
                args: Vec::new(),
            },
            listcontainers: CommandConfig {
                name: "docker".to_string(),
                args: Vec::new(),
            },
            execute: CommandConfig {
                name: "docker".to_string(),
                args: Vec::new(),
            },
        }
    }

    #[test]
    fn valid_config() {
        assert!(Backend::validate_config(&get_config()).is_ok());
    }

    macro_rules! test_empty_command {
        ($field:tt, $fieldname: expr) => {
            let mut config = get_config();
            config.$field.name.clear();
            let err = Backend::validate_config(&config);
            assert!(err.is_err());

            match err.err().unwrap() {
                Errors::CommandEmpty { name } => {
                    assert_eq!(name, $fieldname)
                }
                _ => {
                    assert!(false)
                }
            }
        };
    }

    #[test]
    fn empty_build_config_invalid() {
        test_empty_command!(build, "Build");
    }

    #[test]
    fn empty_run_config_invalid() {
        test_empty_command!(start, "Start");
    }

    #[test]
    fn empty_clean_config_invalid() {
        test_empty_command!(clean, "Clean");
    }

    #[test]
    fn empty_image_exists_config_invalid() {
        test_empty_command!(listimages, "Listimages");
    }
    #[test]
    fn empty_execute_config_invalid() {
        test_empty_command!(execute, "Execute");
    }
}
