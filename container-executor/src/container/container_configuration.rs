use serde::{Deserialize, Serialize};
use std::path::PathBuf;

use crate::errors::{Errors, ExecResult};

#[non_exhaustive]
#[derive(Deserialize, Serialize)]
pub struct ContainerConfiguration {
    pub name: String,
    pub dependson: Option<String>,
    pub buildfile: Option<PathBuf>,
    pub workdir: Option<PathBuf>,
    pub build_args: Option<Vec<String>>,
    pub run_args: Option<Vec<String>>,
    pub clean_args: Option<Vec<String>>,
    pub backend: String,
    pub dependency: Option<std::boxed::Box<ContainerConfiguration>>,
}

impl ContainerConfiguration {
    pub fn new(name: &str, store: &storage::Storage) -> ExecResult<Self> {
        let mut config: Self = conflib::ConfLoader::new(
            PathBuf::new()
                .join("cont-exec")
                .join("containers")
                .join(format!("{name}.toml")),
            "dev-tools".to_string(),
            false,
        )
        .load_config_with_storage(store)?;
        config.validate_config()?;

        if let Some(dep) = config.dependson.as_ref() {
            config.dependency = Some(std::boxed::Box::new(Self::new(dep, store)?));
        }

        Ok(config)
    }

    pub fn validate_config(&self) -> ExecResult<()> {
        if self.name.is_empty() {
            return Err(Errors::NameNotSet);
        }

        if self.backend.is_empty() && self.dependson.is_none() {
            return Err(Errors::BackendNotSet);
        }

        if self.dependson.is_none() && self.buildfile.is_none() {
            return Err(Errors::BuildfileOrDependsonNotSet);
        }

        if self.buildfile.is_none() && self.dependson.as_ref().unwrap().is_empty() {
            return Err(Errors::BuildfileOrDependsonNotSet);
        }

        if self.dependson.is_none() {
            let buildfile = self.buildfile.as_ref().unwrap();

            if !buildfile.is_absolute() {
                if self.workdir.is_none() {
                    return Err(Errors::WorkdirNotSet);
                }

                let workdir = self.workdir.as_ref().unwrap();

                if !workdir.is_absolute() {
                    return Err(Errors::WorkdirMustBeAbsolute);
                }

                let file = workdir.join(buildfile);
                if !file.is_file() {
                    return Err(Errors::BuildfileDoesNotExists { file });
                }
            } else if !buildfile.is_file() {
                return Err(Errors::BuildfileDoesNotExists {
                    file: buildfile.clone(),
                });
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use storage::TempStorage;

    struct Context {
        config: ContainerConfiguration,
        storage: TempStorage,
    }

    fn get_context() -> Context {
        let storage = storage::get_temporary_storage("cont-config").unwrap();
        let config = ContainerConfiguration {
            name: "test".to_string(),
            dependson: None,
            buildfile: Some(std::path::PathBuf::new().join("dummy.sh")),
            build_args: None,
            run_args: None,
            clean_args: None,
            backend: "super_duper_backend".to_string(),
            workdir: Some(storage.storage.get_cache_dir()),
            dependency: None,
        };

        Context { config, storage }
    }

    fn prepare() -> Context {
        let context = get_context();

        std::fs::File::create(
            context
                .config
                .workdir
                .as_ref()
                .unwrap()
                .join(context.config.buildfile.as_ref().unwrap()),
        )
        .unwrap();

        context
    }

    #[test]
    fn valid_configuratios() {
        let context = prepare();

        context.config.validate_config().unwrap();
    }

    macro_rules! validate_error {
        ($context:tt, $error: pat) => {
            let out = $context.config.validate_config();
            assert!(out.is_err());

            match out.err().unwrap() {
                $error => {}
                _ => {
                    panic!("Unexpected error!");
                }
            }
        };
    }

    #[test]
    fn configuration_without_name_invalid() {
        let mut context = prepare();
        context.config.name.clear();
        validate_error!(context, Errors::NameNotSet);
    }

    #[test]
    fn configuration_without_backend_invalid() {
        let mut context = prepare();
        context.config.backend.clear();
        validate_error!(context, Errors::BackendNotSet);
    }

    #[test]
    fn configuration_without_dependson_or_buildscript_invalid() {
        let mut context = prepare();
        context.config.dependson = None;
        context.config.buildfile = None;
        validate_error!(context, Errors::BuildfileOrDependsonNotSet);

        let mut context = prepare();
        context.config.dependson = Some(String::new());
        context.config.buildfile = None;
        validate_error!(context, Errors::BuildfileOrDependsonNotSet);

        let mut context = prepare();
        context.config.dependson = None;
        context.config.buildfile = Some(std::path::PathBuf::new());
        validate_error!(context, Errors::BuildfileDoesNotExists { file: _ });
    }

    #[test]
    fn config_workdir_not_absolute() {
        let mut context = prepare();
        context.config.workdir = Some(std::path::PathBuf::new());
        validate_error!(context, Errors::WorkdirMustBeAbsolute);
    }

    #[test]
    fn config_relative_build_file_missing() {
        let mut context = prepare();
        context.config.workdir = Some(context.storage.storage.get_config_dir());
        validate_error!(context, Errors::BuildfileDoesNotExists { file: _ });
    }

    #[test]
    fn config_absolute_build_file_missing() {
        let mut context = prepare();
        context.config.buildfile = Some(context.storage.storage.get_config_dir().join("dummy.sh"));
        validate_error!(context, Errors::BuildfileDoesNotExists { file: _ });
    }

    #[test]
    fn config_absolute_build_file_is_directory() {
        let mut context = prepare();
        context.config.buildfile = Some(context.storage.storage.get_config_dir());
        validate_error!(context, Errors::BuildfileDoesNotExists { file: _ });
    }

    #[test]
    fn config_absolute_build_file_exists() {
        let mut context = prepare();
        context.config.buildfile = Some(context.storage.storage.get_cache_dir().join("dummy.sh"));
        context.config.validate_config().unwrap();
    }
}
