pub mod container;
mod errors;

pub type Errors = errors::Errors;
pub type ExecResult<T> = errors::ExecResult<T>;

pub fn build(container: &container::Container, force: bool) -> errors::ExecResult<()> {
    if !force && container.image_exists()? {
        return Ok(());
    }

    container.image_build()
}

pub fn start(container: &container::Container, rebuildimage: bool) -> errors::ExecResult<()> {
    if container.is_running()? {
        return Ok(());
    }
    build(container, rebuildimage)?;

    container.start()
}

pub fn stop(container: &container::Container) -> errors::ExecResult<()> {
    if container.is_running()? {
        return container.stop();
    }

    Ok(())
}

pub fn exec<T: AsRef<str>>(
    container: &container::Container,
    rebuildimage: bool,
    runargs: &[T],
) -> errors::ExecResult<()> {
    start(container, rebuildimage)?;
    container.exec(runargs)
}

pub fn clean(container: &container::Container, recursive: bool) -> errors::ExecResult<()> {
    if container.is_running()? {
        stop(container)?;
    }
    container.image_clean(recursive)
}

pub fn save_default_backends(store: &storage::Storage) -> ExecResult<()> {
    container::save_default_backends(store)
}

#[cfg(test)]
mod tests {
    use std::io::Write;
    use std::path::PathBuf;

    use crate::container::container_configuration::ContainerConfiguration;

    use super::*;
    use storage::TempStorage;

    struct BuildFile {
        content: String,
        path: PathBuf,
    }

    struct ContextBuilder {
        tempstorage: storage::TempStorage,
        containername: String,
        buildfile: Option<BuildFile>,

        dependson: Option<String>,
    }

    impl ContextBuilder {
        fn default(name: &str) -> Context {
            let tempstorage = storage::get_temporary_storage(name).unwrap();
            Self {
                containername: name.to_string(),
                dependson: None,
                buildfile: Some(BuildFile {
                    content: r#"
                        from alpine:latest
                        entrypoint while sleep 1; do true; done"#
                        .to_string(),
                    path: tempstorage
                        .storage
                        .get_cache_dir()
                        .join(name)
                        .join("Dockerfile"),
                }),
                tempstorage,
            }
            .build()
        }

        fn new(name: &str) -> Self {
            let tempstorage = storage::get_temporary_storage(name).unwrap();
            Self {
                tempstorage,
                containername: name.to_string(),
                dependson: None,
                buildfile: None,
            }
        }

        fn with_storage(mut self, storage: TempStorage) -> Self {
            self.tempstorage = storage;
            self
        }

        fn with_dependency(mut self, dep: &str) -> Self {
            self.dependson = Some(dep.to_string());
            self
        }

        fn with_buildfile(mut self, buildfile: BuildFile) -> Self {
            self.buildfile = Some(buildfile);
            self
        }

        fn build(self) -> Context {
            save_default_backends(&self.tempstorage.storage).unwrap();
            self.write_test_docker_file();
            self.write_test_container_config();

            Context {
                container: container::Container::new_with_storage(
                    &self.containername,
                    std::rc::Rc::new(self.tempstorage.storage.clone()),
                )
                .unwrap(),
                storage: self.tempstorage,
            }
        }

        fn write_test_docker_file(&self) {
            if let Some(buildfile) = self.buildfile.as_ref() {
                let parent = buildfile.path.parent().unwrap();
                std::fs::create_dir_all(parent).unwrap();

                let mut writer =
                    std::io::BufWriter::new(std::fs::File::create(buildfile.path.clone()).unwrap());

                writer.write_all(buildfile.content.as_bytes()).unwrap();
            }
        }

        fn write_test_container_config(&self) {
            let config = ContainerConfiguration {
                name: self.containername.clone(),
                dependson: self.dependson.clone(),
                buildfile: if let Some(buildfile) = self.buildfile.as_ref() {
                    Some(buildfile.path.clone())
                } else {
                    None
                },
                workdir: None,
                build_args: None,
                run_args: None,
                clean_args: None,
                backend: "docker".to_string(),
                dependency: None,
            };

            let path = self
                .tempstorage
                .storage
                .get_config_dir()
                .join("cont-exec")
                .join("containers");
            if !path.exists() {
                std::fs::create_dir_all(path.clone()).unwrap();
            }

            let contpath = path.join(format!("{}.toml", self.containername));
            println!("{contpath:?}");
            storage::Storage::write_to_toml(contpath, &config).unwrap();
        }
    }
    struct Context {
        storage: TempStorage,
        container: container::Container,
    }

    impl Drop for Context {
        fn drop(&mut self) {
            clean(&self.container, true).unwrap();
        }
    }

    #[test]
    fn build_test() {
        let context = ContextBuilder::default("buildtest");
        assert!(!context.container.image_exists().unwrap());
        build(&context.container, true).unwrap();
        assert!(context.container.image_exists().unwrap());
    }

    #[test]
    fn depends_test() {
        let _contextbase = ContextBuilder::default("dependsbase");
        let contextextended = ContextBuilder::new("dependsextended")
            .with_storage(_contextbase.storage.clone())
            .with_dependency("dependsbase")
            .with_buildfile(BuildFile {
                content: r#"
                    from dependsbase
                    run mkdir -p /test
                    copy testfile /test/testfile
                    "#
                .to_string(),
                path: _contextbase
                    .storage
                    .storage
                    .get_cache_dir()
                    .join("dependsextended")
                    .join("Dockerfile"),
            })
            .build();
        let contextfinal = ContextBuilder::new("final")
            .with_storage(_contextbase.storage.clone())
            .with_dependency("dependsextended")
            .build();

        std::fs::File::create(
            contextextended
                .storage
                .storage
                .get_cache_dir()
                .join("dependsextended")
                .join("testfile"),
        )
        .unwrap();
        assert!(!contextfinal.container.image_exists().unwrap());
        assert!(exec(
            &contextfinal.container,
            true,
            &["test", "-f", "/test/testfile42"],
        )
        .is_err());
        exec(
            &contextfinal.container,
            true,
            &["test", "-f", "/test/testfile"],
        )
        .unwrap();
        clean(&contextfinal.container, false).unwrap();
        assert!(contextfinal.container.image_exists().unwrap());
        clean(&contextfinal.container, true).unwrap();
        assert!(!contextfinal.container.image_exists().unwrap());
    }

    #[test]
    fn start_test() {
        let context = ContextBuilder::default("starttest");
        start(&context.container, true).unwrap();
        assert!(context.container.is_running().unwrap());
    }

    #[test]
    fn exec_test() {
        let context = ContextBuilder::default("exectest");
        exec(&context.container, true, &["echo", "succes"]).unwrap();
        assert!(context.container.is_running().unwrap());
    }
}
